#!/bin/bash

#the database file name is stored in the variable "DATABASE"
# the original arguments are passed to functions, so you can parse the
# arguments on your own

function listCourses {
    # remove this echo and put your code here
    (cat $DATABASE | grep "$2") | awk -F ',' '{print $4}'
}

listCourses "$@"
