#!/bin/bash

#the database file name is stored in the variable "DATABASE"
# the original arguments are passed to functions, so you can parse the
# arguments on your own

function delete {
    # remove this echo and put your code here
    shift
    STR=`grep "$1" $DATABASE | grep "$2"`
    grep -v "$STR" $DATABASE > temp
    mv temp $DATABASE
    echo "The total number of records is:"
    cat $DATABASE | wc -l | tr -d ' '

}

delete "$@"
