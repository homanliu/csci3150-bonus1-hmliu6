#!/bin/bash

#the database file name is stored in the variable "DATABASE"
# the original arguments are passed to functions, so you can parse the
# arguments on your own

function insert {
    # remove this echo and put your code here
    shift
    for ((i=1; i<=$#; i++)); do
      if [ "$i" != "4" ];
      then
        echo -n "${!i}", >> $DATABASE
      else
        echo "${!i}" >> $DATABASE
      fi
    done
    # cat $DATABASE
    echo "The total number of records is:"
    cat $DATABASE | wc -l | tr -d ' '
}

insert "$@"
