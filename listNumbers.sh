#!/bin/bash

#the database file name is stored in the variable "DATABASE"
# the original arguments are passed to functions, so you can parse the
# arguments on your own

function listNumbers {
    # remove this echo and put your code here
    shift
    for i in "$@"; do
      (cat $DATABASE | grep "$i") | wc -l | tr -d ' '
    done
}

listNumbers "$@"
